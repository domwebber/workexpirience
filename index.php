<?php
	require_once('includes/functions.php');
	show_header(array(
		'pageTitle' => "Welcome to Webber's world",
		'pageDescription' => "This is a website dedicated to learning more about PHP and the world wide web."
	));
	require_once("includes/navbar.php");
?>

<h1>Welcome Page 2!</h1>

<?php require_once('includes/footer.php'); ?>
