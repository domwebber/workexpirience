module.exports = function(grunt){

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
 
		/* Sass Task */
		sass: {
			dev: {
				options: {
					style: 'expanded',
					sourcemap: 'none',
				},
				files: {
					'assets/compiled/style-human.css': 'assets/sass/style.scss',
				}
			},
			
			dist: {
				options: {
					style: 'compressed',
					sourcemap: 'none',
				},
				files: {
					'assets/compiled/style.css': 'assets/sass/style.scss',
				}
			}
		},


		/* Autoprefixer */
		autoprefixer: {
			options: {
				browsers: ['last 3 versions']
			},

			/* Prefix the files */
			multiple_files: {
				expand: true,
				flatten: true,
				src: 'assets/compiled/style.css',
				dest: ''
			}
		},

		/* Watch Task */
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass', 'autoprefixer']
			}
		}

	});

	/* Load Grunt Modules */	
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.registerTask('default', ['watch']);
}
