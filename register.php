<?php
	require_once('includes/functions.php');


		$user = new user();
		$return_message = $user->register_account($_POST);

	$pageTitle = "Register for Webber's world";

	show_header(array(
		"pageTitle"=>$pageTitle,
		"pageDescription"=>"Register for Dom Webber's work experience website.")
	);
	require_once("includes/navbar.php");
?>
		<div class="container">
        		<div class="card card-container">
							<?php echo show_page_title($pageTitle); ?>
				<form class="form-signin" action="" method="POST">
					<div class="login-error"><?php if(!empty($return_message)){echo $return_message;}?></div>
					<select name="title" class="form-control" placeholder="Title" >
						<option value="Mr">Mr</option><option value="Miss">Miss</option><option value="Mrs">Mrs</option>
					</select>
					<label for="inputFName" class="sr-only">First Name:</label>
					<input type="text" name="first_name" id="inputFName" class="form-control" placeholder="First Name"  autofocus>
					<label for="inputLName" class="sr-only">Last Name:</label>
					<input type="text" name="last_name" id="inputLName" class="form-control" placeholder="Last Name"  autofocus>
					<label for="inputEmail" class="sr-only">Email Address:</label>
					<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address"  autofocus>
					<label for="inputPassword" class="sr-only">Password:</label>
					<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" >
					<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="register" value="submit">Register</button>
				</form>
			</div>
		</div>
<?php require_once('includes/footer.php'); ?>
