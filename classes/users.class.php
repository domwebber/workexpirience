<?php
/*
 *  Description: This is the users class that handles user interactions with the website
 *  Date: 16/07/18
 *
 *  Tomorrow: Work on Admin area
 */



class user extends database {

  // static means that you have one reference to the variable across all instances of your class
  protected $tableName = 'users';

  //We should have class properties for every column in our table
  private $title;
  private $first_name;
  private $last_name;
  private $email;
  private $created;
  private $password;
  private $role_id;
  private $blocked;
  private $account_notes;

  protected $tableColumns = ['title', 'first_name', 'last_name', 'email', 'password'];


  /**
   * Functions to get and set protected variables that are only accessible by this class
   * @return array Array of columns used in the users table
   */
  public function getTableColumns(){
    return $this->tableColumns;
  }

  function __construct(){
    // Run the parent class's constructor so we can connect to the database
    parent::__construct();

    // $this->tableColumns = parent::getTableCols($this->tableName,'array',true);
  }


	/*
	    Update Details
	    Store Preferences
	    Reset Password
	    Verify Account
	*/

  public function get_account($id,$email){
    $sql = "SELECT * FROM {$this->tableName} WHERE `id`='{$id}' LIMIT 1;";
    $result = parent::select($sql)->fetch_assoc();

    return $result;
  }

  public function getAccountById($id){
    return parent::select("SELECT * FROM {$this->tableName} WHERE `id`='$id' LIMIT 1;")->fetch_assoc();
  }

  public function register_account($data){
    echo '<h2>Login Function</h2>';
    $error = null;

    if(!isset($_POST['register'])):
      return;
    endif;

    // Use the $_POST
    foreach($this->tableColumns as $req):
      //loop though each required field and check there's a value for /**
       if(empty($data[$req])):
         $error .= $req . " cannot be left empty!<br>";
       endif;
    endforeach;

    if(!empty($error)){
      return $error;
      //Don't carry on inserting into the database if there's an error
    }

		//Hash password field only
		$passwordLen = strlen($data['password']);

    $columns = implode(', ', $this->tableColumns);
    $values = array();
    $vals = $this->getTableColumns();

    // what is this doing? Comment your code!!
    foreach($vals as $v){
      if(!isset($data[$v]) || empty($data[$v])){
        //Empty value allowed
        $values[$v]="null";
      }else{
        //insert value
        if($v == "password"){
          $values["password"] = password_hash(clean($data["password"]),PASSWORD_BCRYPT);
        }else{
          $values[$v] = clean($data[$v]);
        }
      }
    }

		//I'm going to be using the email as an unique column too, so check for other users by the same email
		if(parent::select("SELECT id FROM ".$this->tableName." WHERE `email`='".$data['email']."' LIMIT 1;")->num_rows > 0):
			return "That email is already taken.";
		elseif(!filter_var($data['email'],FILTER_VALIDATE_EMAIL)):
			return "The email you submitted was not valid.";
		elseif($passwordLen <= 5):
			return "Password must be at least 6 characters long.";
		elseif(parent::insert($values)):
			return redirect("./login.php?ref=register");
		else:
			return "There was an error creating your account, please try again later.";
		endif;
	}

	public function login($redirect=true){
    echo '<h2>Login Function</h2>';
    if(!isset($_POST['loginBtn'])):
      return;
    endif;

    $post = $_POST['login'];

		// Sanitise
    foreach($post as $element):
      $post[$element] = clean($element);
      //$email = htmlspecialchars($email, ENT_QUOTES, 'UTF-8');
    endforeach;

		//Request login - Keep query for num_rows
		$query = parent::select("SELECT * FROM `".$this->tableName."` WHERE `email` = '".$post['email']."' AND `Blocked` = '0'");

    $result = $query->fetch_assoc();


		//Login (error) message
		if($query->num_rows > 0 && isset($result['password']) && password_verify(clean($post['password']),$result['password'])){
			//User's login is verified, so allow the user access to their account
			$_SESSION['userId'] = $result['id'];
			$_SESSION['userEmail'] = $result['email'];
      $_SESSION['userRole'] = $result['role_id'];
      $_SESSION['userColour'] = parent::select("SELECT * FROM `user_settings` WHERE `user_id` = '".$result['id']."';")->fetch_assoc()['value'];
			if($redirect){
				return redirect("./dashboard/");
			}else{
				return true;
			}
		}else{
			//Oh no, something was wrong...
			if($redirect){
				return redirect("./login.php?err=login");
			}else{
				return false;
			}
		}
	}

  public function setting($data){
    $setting = [];
    if(isset($data['color'])){
      $setting['setting_id'] = "3";
      $setting['user_id'] = $_SESSION['userId'];
      $setting['value'] = $data['color'];
    }
    database::update($setting,$table='user_settings');
  }

  //Update the user's selected information in the database
  public function change_details($data,$id=0){
    $okay = true;

    $selectedAccount = $id;

    unset($data['id']);

    if(!is_array($data)):
  		exit('Parameters must be an array');
  	endif;

    $set = [];

    foreach($data as $key => $value){
      if($key=="password" && strlen($value) > 5){$value=password_hash(clean($value),PASSWORD_BCRYPT);}else{$okay=false;}
      if($key=="email" && !filter_var($value,FILTER_VALIDATE_EMAIL)){return "Invalid email.";$okay=false;}
      if( isset( $value ) && in_array( $key,database::getTableCols('users','array',false) ) ){
        array_push($set,("`".$key."`='".clean($value)."'"));
      }

      print_r($set);

    if(in_array($value,$this->tableColumns) && !empty($detail) && !empty($value) && $okay && $id):
      parent::update("UPDATE `users` SET ".join(',',$set)." WHERE `id`='$selectedAccount';");
    endif;
  }
}

public function get_settings($id){
  $selected = parent::select("SELECT * FROM `user_settings` WHERE `user_id`='$id';");
  return $selected->fetch_all();
}

  //Account deletion DELETE function
  public function delete_account($id,$password){
    $query = parent::select("SELECT * FROM `".parent::tableName."` WHERE `id` = '$id'");
    $result = $query->fetch_assoc();

    if($query->num_rows > 0 && isset($result['password']) && password_verify($password,$result['password'])){
			//Delete user's account if they have valid information
      parent::delete("DELETE FROM `".parent::tableName."` WHERE `id`='$id';");
      return "Account deleted successfully.";
    }else{
      return "There was an error deleting your account. Please try again.";
    }
  }

  public function logged_in(){
    if(isset($_SESSION['userId'])):
      return true;
    else:
      return false;
    endif;
  }
}
