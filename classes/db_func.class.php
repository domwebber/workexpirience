<?php

class database {

  private $conn;
  private $config;
  private $db_host;
  private $db_user;
  private $db_pass;
  private $db_name;
  private $db_port;


  //Initiate database connection for use in other functions
	function __construct(){
    // Load the configuration file so our class has access to the database. The true parameted on the end is there because we have sections [database] for example.
    $this->config = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . '/includes/config.ini', true);

    // Set the class configuration to whatever is specified in the configuration file
    $this->db_host = $this->config['database']['DB_HOST'];
    $this->db_user = $this->config['database']['DB_USER'];
    $this->db_pass = $this->config['database']['DB_PASS'];
    $this->db_name = $this->config['database']['DB_NAME'];

    // Connect to the database and store the connection in $this->conn so that this class and inherted classes have access to this
    $this->conn = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
	}


  // Query the database and get the list of table columns
  public function getTableCols($tableName, $returnType='array',$required=false){
    if($required){
      $sql = "SELECT COLUMN_NAME FROM `information_schema`.`COLUMNS` WHERE table_schema='localhost' AND table_name='".$tableName."' AND `COLUMN_COMMENT`='required';";
    }else{
      $sql = "SHOW COLUMNS FROM {$tableName}";
    }
    $query = $this->conn->query($sql);
    $columns = $query->fetch_all();

    // work out if the returnType is array or list and based upon that, you want to return either an array of columsns or a comma separated list
    $cols = array();

    // Build up an array of column names
    foreach($columns as $col):
      array_push($cols, $col[0]);
    endforeach;
    // End array/list

    if($returnType=="array"){
      return $cols;
    }else{
      return join(',',$cols);
    }
  }

  public function getAllRows($joined=false){
    //get all rows from users database
    if($joined):
      //$joined
      $sql = $this->select("SELECT `user_roles`.`name`,`users`.* FROM `user_roles` INNER JOIN `users` ON `user_roles`.`id`=`users`.`role_id`;");
    else:
      $sql = $this->select("SELECT * FROM `users`;");
    endif;

    return [$sql->fetch_all(),$sql->num_rows];
  }


/**
 * Get the user from the database based on an ID
 * @param  int  $id  the User's ID
 * @return mysqli Mysqli result
 */
  public function getById($id){
    // Only run the code if the ID is an integer
    if(intval($id) !== 0):
      $id = intval($id);
      $sql = $this->select("SELECT `user_roles`.`name` as user_role,`users`.* FROM `user_roles` INNER JOIN `users` ON `user_roles`.`id`=`users`.`role_id` WHERE `users`.`id`='$id' LIMIT 1;");
      return $sql->fetch_assoc();
    else:
      throw new Exception('ID parameter is not an integer');
    endif;
  }
  // Select data from the database
  public function select($sql){
    // Select statement from sql
    //$sql = "SELECT $columns FROM `users`";
    return $this->conn->query($sql);
  }

  // Insert data into the database
  public function insert($data){

    $columns = [];
    $values = [];

    // build insert statement based on $data array
    foreach($data as $key => $value){
      array_push($columns,"`".htmlspecialchars($key,ENT_QUOTES)."`");
      if($value != "null"){
        array_push($values,'\''.$value.'\'');
      }else{
        if($key == "role_id"){
          array_push($values,"'1'");
        }else{
          array_push($values,"null");
        }
      }
    }

    $sql = "INSERT INTO `users` (".join(',',$columns).") VALUES (".join(',',$values).");";

    echo $sql;

    var_dump(password_verify($data['password'],$hashed));

    return $this->conn->query($sql);
  }

  //Delete data from the database
  public function delete($sql){
    //Delete statement
    return $this->conn->query($sql);
  }

  //Update data in the database
  public function update($data,$id=null,$table="users"){
    //Update statement

    $sets = [];

    foreach($data as $key => $value){
      array_push($sets," `$key` = '$value'");
    }

    $sql = "UPDATE `$table` SET " . join(',',$sets);
    if(isset($id)){$sql.=" WHERE `id`='$id'";}

    return $this->conn->query($sql);
  }
}
