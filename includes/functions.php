<?php

session_start();

/*Functions file*/
require_once('/var/www/domwebber/public_html/classes/db_func.class.php');
require_once('/var/www/domwebber/public_html/classes/users.class.php');


if(!function_exists('show_page_title')):
	function show_page_title($pageTitle){
		// Best practice to set the variables before they are returned or modified.
		$title = null;

		if(isset($pageTitle) && !empty($pageTitle)):
			$title =  "<h1>{$pageTitle}</h1>";
		endif;

		return $title;
	}
endif;


function show_header($data){
	// Our header data must be in an array format
	if(!is_array($data)):
		exit('Header Parameters must be an array');
	endif;

	// There are variables within includes/header.php that will be loaded from the values passed into the show_header function
	return require_once('/var/www/domwebber/public_html/includes/header.php');
}

/**
 * Short way of sanitizing inputs
 * @param  string $str the string that is passed in to be sanitized
 * @return string the sanitized output of the clean function
 */
function clean($str){
	$clean = htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
	return $clean;
}

/**
 * Redirect site users
 * @param  string $url The destination url for the redirect
 * @return redirect Returns the execution of the redirect script, taking the user to the url specified in the $url
 */
function redirect($url){
	return header("Location: " . $url );
}

function user_colour(){
	if(!isset($_SESSION['userId'])){
		return null;
	}else{
		$colour = (new database)->select("SELECT * FROM `user_settings` WHERE `user_id` = '".$_SESSION['userId']."';")->fetch_assoc()['value'];
		$_SESSION['userColour'] = $colour;
		return $colour;
	}
}
