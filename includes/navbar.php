<nav class="navbar navbar-expand-md navbar-dark bg-dark" id="navbar-nav" <?php

                if(!empty(user_colour())){
                  echo " style='background:".$_SESSION['userColour']." !important;'";
                }

?>>
                        <a class="navbar-brand" href="/">Dom Webber</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                                <ul class="navbar-nav mr-auto">
                                        <li class="nav-item active">
                                                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                                        </li>
					<li class="nav-item">
						<a class="nav-link" href="/about.php">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/twitter.php">Twitter</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/instagram.php">Instagram</a>
					</li>
                                </ul>
                                <ul class="navbar-nav">
					<?php if(!isset($_SESSION['userId'])):?>
                                        	<li class="nav-item">
                                        	        <a class="nav-link" href="/login.php">Login</a>
                                        	</li>
						                              <li class="nav-item">
                							                     <a class="nav-link" href="/register.php">Register</a>
                              						</li>
					<?php else:?>
						<li class="nav-item">
							<a class="nav-link" href="/dashboard/">Dashboard</a>
						</li>
					<?php endif;?>
                                </ul>
                        </div>
                </nav>
