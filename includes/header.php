<!Doctype html>
<html lang="EN">
        <head>
                <title><?php echo $data['pageTitle']; ?></title>
                <link rel="icon" href="/favicon.png" type="image/png">
                <!--Author: Dom Webber-->

		<!--Viewport-->
		<media content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
		<!--Viewport-->

                <!--Bootstrap-->
                <link rel="stylesheet" href="/style.css">
                <script src="/assets/js/jquery-3.3.1.js"></script>
                <script src="/assets/js/bootstrap.min.js"></script>
                <!--Bootstrap-->
        </head>

        <body>
