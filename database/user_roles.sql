CREATE TABLE `user_roles` (
 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
 `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
 `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
 UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
