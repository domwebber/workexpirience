CREATE TABLE `users` (
 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
 `title` text COLLATE utf8mb4_unicode_ci,
 `first_name` text COLLATE utf8mb4_unicode_ci,
 `last_name` text COLLATE utf8mb4_unicode_ci,
 `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `password` text COLLATE utf8mb4_unicode_ci,
 `role_id` int(11) NOT NULL DEFAULT '0',
 `blocked` tinyint(1) NOT NULL DEFAULT '0',
 `account_notes` text COLLATE utf8mb4_unicode_ci,
 UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='User Account Information'
