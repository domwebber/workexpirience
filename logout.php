<?php

// Remove all session stored variables
session_start();
unset($_SESSION);

//Log out
session_destroy();

//Redirect to the home page
header('Location: /?ref=logout');
