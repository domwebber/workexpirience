<?php
	require_once('includes/functions.php');

	$pageTitle = "Login to Webber's world";
	show_header(array(
		"pageTitle" => $pageTitle,
		"pageDescription" => "Login to Dom Webber's work experience website."
	));

	require_once("includes/navbar.php");

	// If the form has been posted, instantiate a new user and log them in.
	// The login function will sanitise the inputs and log the user in
	if(isset($_POST['loginBtn'])):
		$user = new user();
		$user->login();
	endif;
?>

		<div class="container">
        		<div class="card card-container">
				<!-- Do not repeat yourelf: this would be easier to maintain if it was part of a function -->
				<?php echo show_page_title($pageTitle); ?>

				<!-- Use localStorage to store their image path so if their session comes to an end their image is available when they come to login again -->
				<img class="card-img-top" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />


				<form class="form-signin" action="" method="POST">
					<div class="login-error"><?php if(isset($_GET['err'])){echo "Incorrect email and/or password.";}?></div>
					<label for="inputEmail" class="sr-only">Email Address:</label>
					<input type="email" name="login[email]" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
					<label for="inputPassword" class="sr-only">Password:</label>
					<input type="password" name="login[password]" id="inputPassword" class="form-control" placeholder="Password" required>
					<div id="remember" class="checkbox">
						<label>
							<input type="checkbox" name="login[remember]" value="remember-me"> Remember me
						</label>
					</div>
					<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="loginBtn">Login</button>
				</form>
				<a href="#" class="forgot-password">Forgot the password?</a>
			</div>
		</div>
<?php require_once('includes/footer.php'); ?>
