<?php

	// This will contain a table that lists the users from the database, with a column at the end for actions (edit, delete)


	// Show a header (Users) using out header function/include
	// Include a button to add a new record

	// Get a list of all users from the database
	// Output the table headers

	// Find out idf any data was returned from the get all function - if nothing was returned we want to show "No records found" inside the table

	// Otherwise, if we have records then we want a foreach loop that goes through the records and outputs the data
	// Optional: Table footer showing total record count
	?>

	<?php
		require_once('../../includes/functions.php');

	  $pageTitle = "Admin Webber's world";
		show_header(array(
			'pageTitle' => $pageTitle,
			'pageDescription' => "This is a website dedicated to learning more about PHP and the world wide web."
		));
		require_once('../../includes/navbar.php');

		if(!empty($_SESSION['userId']) && !empty($_SESSION['userEmail'])):
			$logged = (new user())->get_account($_SESSION['userId'],$_SESSION['userEmail']);
			if($logged['role_id'] != 2):
				header("Location: /dashboard/");
			endif;
		else:
			header("Location: ../login.php");
		endif;

	?>

	<?php echo show_page_title($pageTitle); ?>

	<?php echo "<div class='table-responsive'><table class='table-bordered table-striped table-condensed'>";

  $database = new database();
	$s = $database->getAllRows();

if($s[1] > 0):

	echo "<thead class='thead-dark'>";
	$users = new User();
	$c = $users->getTableColumns();

	echo "<tr>";
	echo "<th scope='col'>Id</th>";
	foreach($c as $t){
		echo "<th scope='col'>".$t."</th>";
	}
	echo "</tr>";
	echo "</thead>";

	echo "<tbody>";
	foreach($s[0] as $rows):
		//Echo data
		echo "<tr>";
		foreach($rows as $key => $value):
			//Echo each
			echo "<td>" . $value . "</td>";
		endforeach;
		echo "</tr>";
	endforeach;
	echo "</tbody>";

	echo $s[1] . " user(s)";
else:
	echo "<tr><td><h3>No results</h3></td></tr>";
endif;
echo '</table></div>';

require_once('../../includes/footer.php'); ?>
