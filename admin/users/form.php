<?php
	// This will contain your form that will be used for the add AND edit page
?>
  <div class="container">
          <div class="card card-container">
            <?php echo show_page_title($pageTitle); ?>
      <form class="form-signin" action="" method="POST">
        <?php if(isset($id)){echo "<input type='number' hidden name='id' value='$id'>";}?>
        <div class="login-error"><?php if(isset($return_messag)){echo $return_messag;}?></div>
        <select name="title" class="form-control" placeholder="Title" >
          <option value="Mr" <?php if($r['title'] == "Mr"){echo "Selected";} ?>>Mr</option>
          <option value="Miss" <?php if($r['title'] == "Miss"){echo "Selected";} ?>>Miss</option>
          <option value="Mrs" <?php if($r['title'] == "Mrs"){echo "Selected";} ?>>Mrs</option>
        </select>
        <label for="inputFName" class="sr-only">First Name:</label>
        <input type="text" name="first_name" id="inputFName" class="form-control" placeholder="First Name" value="<?php echo $r['first_name']; ?>"  autofocus>
        <label for="inputLName" class="sr-only">Last Name:</label>
        <input type="text" name="last_name" id="inputLName" class="form-control" placeholder="Last Name" value="<?php echo $r['last_name']; ?>" autofocus>
        <label for="inputEmail" class="sr-only">Email Address:</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" value="<?php echo $r['email']; ?>" autofocus>
        <label for="inputPassword" class="sr-only">Password:</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" >
        <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="register" value="submit">Save</button>
      </form>
    </div>
  </div>
