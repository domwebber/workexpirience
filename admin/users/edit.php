<?php

require_once('../../includes/functions.php');

$pageTitle = "Admin Webber's world";
show_header(array(
  'pageTitle' => $pageTitle,
  'pageDescription' => "This is a website dedicated to learning more about PHP and the world wide web."
));
require_once("../../includes/navbar.php");

$id = htmlspecialchars($_GET['id']);

		$user = new user();

	// This will allow you to edit the user details
  if( isset($_POST['register']) ):

		$return_message = (new user)->change_details($_POST,$_POST['id']);
	endif;

  if( isset($_GET['id']) ){
    $r = $user->getAccountById($id);
  }

  require_once("form.php");
