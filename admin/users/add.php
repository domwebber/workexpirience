<?php

	require_once('../../includes/functions.php');

  $pageTitle = "Admin Webber's world";
	show_header(array(
		'pageTitle' => $pageTitle,
		'pageDescription' => "This is a website dedicated to learning more about PHP and the world wide web."
	));
	require_once("../../includes/navbar.php");

	// This will add a new user to the website
  if(isset($_POST['register'])):
		$user = new user();
		$return_message = $user->register_account($_POST);
	endif;

  require_once("form.php");
