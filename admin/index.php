<?php
	require_once('../includes/functions.php');

  $pageTitle = "Admin Webber's world";
	show_header(array(
		'pageTitle' => $pageTitle,
		'pageDescription' => "This is a website dedicated to learning more about PHP and the world wide web."
	));
	require_once("../includes/navbar.php");

	if(!empty($_SESSION['userId']) && !empty($_SESSION['userEmail'])):
		$logged = (new user())->get_account($_SESSION['userId'],$_SESSION['userEmail']);
		if($logged['role_id'] != 2):
			header("Location: /dashboard/");
		endif;
	else:
		header("Location: ../login.php");
	endif;

?>

<?php echo show_page_title($pageTitle." as ".htmlspecialchars($_SESSION['userEmail'],ENT_QUOTES) ); ?>

<?php require_once('../includes/footer.php'); ?>
