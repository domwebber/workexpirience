<?php
	class Shipping_Countdown {
		// This will store the date and time used for the countdown time
		public $countdown_datetime;

		// This will store a debug message that is shown in the debug() function
		protected $debug_message;

		// This stores the CSS background colour that we use to see if the countdown logic is working, it is only used for debug purposes
		public $css_background;

		// This stores the current date and time value using date()
		public $current_date_time;

		// This stores the day value (monday, tuesday etc) for today's date so we can find the cutoff time
		public $today;

		// This stores the day value (monday, tuesday etc) for tomorrow's date so we can find the cutoff time
		public $tomorrow;

		// This stores the date/time of the cutoff time today
		public $delivery_cut_off_time_today;

		// This stores the date/time of the cutoff time tomorrow
		public $delivery_cut_off_time_tomorrow;

		// This stores a bit of text that is shown above the countdown timer
		public $text_above_countdown;

		// This stores a bit of text that is shown below the countdown timer
		public $text_below_countdown;


		/**
		 * [__construct Set the necessary class properties used for the countdown timer]
		 */
		public function __construct(){
			// Specify the default timezone so we use that explicit time rather than getting the current time from the server
			date_default_timezone_set('Europe/London');

			// Set the variables
			$this->current_date_time = date('Y-m-d H:i:s');

			// Set the text above and below the countdown
			$this->text_above_countdown = 'Next Day Delivery Countdown';
			$this->text_below_countdown = 'Delivery is subject to our <a href="#">delivery terms and conditions</a>.';
			$this->delivery_unavailable_message = 'Next day delivery is unavailable!';

			// Get the the day for today and tomorrow (tuesday, wednesday etc, l = day in lowercase)
			$this->today = strToLower(date('l'));

			$this->tomorrow = strToLower(date('l', strtotime('tomorrow')));
			
			// Get an array of cutoff times for next day delivery
			$cutoff_times = self::get_cutoff_times();

			// Find the cutoff time from the array
			// it should find the day in the array key and return the value
			$this->delivery_cut_off_time_today = $cutoff_times[$this->today];
			$this->delivery_cut_off_time_tomorrow = $cutoff_times[$this->tomorrow];
		}
		
		
		/**
		 * [get_cutoff_times Get the delivery cut-off time that are set by the website owner]
		 * This is a static function of the class that can be called without needing to create an instance of the class
		 * @return array returns an array of weekdays with a cutoff time for each day
		 */
		static public function get_cutoff_times(){
			$cutoff_times = array(
				'monday' => '1pm',
				'tuesday' => '2pm',
				'wednesday' => '3pm',
				'thursday' => '4pm',
				'friday' => '8am',
				'saturday' => '',
				'sunday' => '4.30pm',
			);

			return $cutoff_times;
		}


		/**
		 * Returns debug information for the countdown timer
		 * @return string Returns the markup to display the debug information
		 */
		public function debug(){	
			$return = '<div class="debug">';				
				$return .= '<h3>Countdown Debug Info:</h3>';
				$return .= '<div class="message" style="background:' . $this->css_background  . ';">' . $this->debug_message . '</div>';
				
				$return .= '<br/>Time Now: ' . $this->current_date_time . '<br/>';
				$return .= 'Delivery cut-off time (today - ' . $this->today . '): ' . $this->delivery_cut_off_time_today . '<br/>';
				$return .= 'Delivery cut-off time (tomorrow ' . $this->tomorrow . '): ' . $this->delivery_cut_off_time_tomorrow . '<br/>';
				$return .= '$this->countdown_datetime: ' . $this->countdown_datetime . '<br/>';
			$return .= '</div>';

			return $return;
		}


		/**
		 * [show_countdown_timer Use the class properties that are set in the constructor to show a countdown timer]
		 * @return [type] [description]
		 */
		public function show_countdown_timer(){
			
			$cutoff_time_today = strtotime( date('H:i:s', strtotime($this->delivery_cut_off_time_today)) );
			$current_time = time();

			// If the current date/time is less than the cutoff time then countdown until the cutoff time
			if($current_time <= $cutoff_time_today):

				$this->countdown_datetime = date('Y-m-d H:i:s', strtotime('today ' . $this->delivery_cut_off_time_today));
				$this->debug_message = "Next day delivery is available today until {$this->delivery_cut_off_time_today}";
				$this->css_background  = '#1fb515';

			// If the current date/time is greater than the cutoff time for today then we need to countdown to tomorrow's cutt-off time
			elseif($current_time >= $cutoff_time_today):
				
				$this->countdown_datetime = date('Y-m-d H:i:s', strtotime('tomorrow ' . $this->delivery_cut_off_time_tomorrow));
				$this->debug_message = "The current time is past today's cut-off time. Next day delivery is available tomorrow: " . date('d-m-Y H:i:s', strtotime('tomorrow ' . $this->delivery_cut_off_time_tomorrow));
				$this->css_background  = '#ff4c4c';

			// If no next delivery available
			else:

				// Set the datetime property to 0 so we show the error message and not the countdown
				$this->countdown_datetime = 0;
				$this->css_background  = '#ff4c4c';
				$this->debug_message = "The current time is past today's cut-off time and next day delivery is not available tomorrow";

			endif;

			
			// Show the countdown timer 
			if($this->countdown_datetime != 0):
				echo '<div class="countdown-timer-container half">';
					echo '<div class="countdown-timer">';
						if(!empty($this->text_above_countdown)): 
							echo '<div class="above-timer">' . $this->text_above_countdown . '</div>'; 
						endif;

						echo '<div id="countdownClock" class="countdown">';
							echo '<div class="hour">';
								echo '<div class="number">10</div>';
								echo '<p>Hours</p>';
							echo '</div>';

							echo '<div class="minute">';
								echo '<div class="number">0</div>';
								echo '<p>Minutes</p>';
							echo '</div>';

							echo '<div class="second">';
								echo '<div class="number">0</div>';
								echo '<p>Seconds</p>';
							echo '</div>';
						echo '</div>';

						if(!empty($this->text_below_countdown)): 
							echo '<div class="below-timer">' . $this->text_below_countdown . '</div>'; 
						endif;
					echo '</div>';
				echo '</div>';
			else:
				// Next day delivery is not available if a null value exists for today and tomorrow cut-off times
				echo '<div class="countdown-timer-container half">';
					echo '<div class="delivery-unavailable">';
						echo $this->delivery_unavailable_message;
					echo '</div>';
				echo '</div>';
			endif; // end time validation check
		}
	}