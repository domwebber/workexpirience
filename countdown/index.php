<html>

	<head>
		<style type="text/css">
			.debug { background:#EEE; border:1px solid #DDD; padding:10px; margin:35px 0; }
			.debug .message { clear:both; overflow:hidden; display:inline-block; margin:15px 0; padding:5px 10px; color:#FFF; font-weight:bold; }
		</style>

	</head>

	<body>
		<?php include('inc/countdown.class.php'); ?>


		<?php
			$countdown = new Shipping_Countdown();

			// Show the Countdown Timer
			echo $countdown->show_countdown_timer();

			// Show the debug information for the current timers
			echo $countdown->debug();	

		?>


		<script src="/js/countdown.js" type="text/javascript"></script>
	</body>
</html>