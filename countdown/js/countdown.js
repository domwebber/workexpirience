var hours = null,mins = null,secs = null,t = 0;

function parseTime( e1, e2, e3 ){
	if(hours == null){
		hours = parseInt(e1.innerHTML);
		mins = parseInt(e2.innerHTML);
		secs = parseInt(e3.innerHTML);
		
		t = (hours*3600) + (mins*60) + secs;
	}
	
	t -= 1;
	
	console.log(t);
	
	h = Math.floor(t / 3600);
	hr = t % 3600;
	
	m = Math.floor(hr / 60);
	mr = hr % 60;
	
	s = Math.floor(mr);
	
	if(h < 10){
		h = "0" + h;
	}
	if(m < 10){
		m = "0" + m;
	}
	if(s < 10){
		s = "0" + s;
	}
	
	e1.innerHTML = h;
	e2.innerHTML = m;
	e3.innerHTML = s;
	
	if(t > 0){
		setTimeout(function(){parseTime(e1,e2,e3);},1000);
	}
}

function loadListen(elem,callback){
	if (elem.addEventListener) {                    // For all major browsers, except IE 8 and earlier
		elem.addEventListener("load",callback);
	} else if (elem.attachEvent) {                  // For IE 8 and earlier versions
		elem.attachEvent("onload",callback);
	}else{  //  Fallback;
		elem.onload = callback;
	}
	
}

loadListen(window,function(){
	parseTime( document.getElementById("h"), document.getElementById("m"), document.getElementById("s") );
});