<?php
	require_once('../includes/functions.php');

	$pageTitle = "Webber's world Dashboard";
	show_header(array(
		"pageTitle" => $pageTitle,
		"pageDescription" => "Account dashboard for Dom Webber's work experience website."
	));

	require_once("../includes/navbar.php");

	if(!empty($_SESSION['userId']) && !empty($_SESSION['userEmail'])):
		$logged = (new user())->get_account($_SESSION['userId'],$_SESSION['userEmail']);
		if($logged['role_id'] == 2):
			header("Location: /admin/");
		endif;
	else:
		header("Location: /login.php");
	endif;
?>

<?php require_once('../includes/footer.php'); ?>
<h1>Logged in as <?php echo $_SESSION['userEmail']; ?>!</h1>

<?php

var_dump(user::get_settings($_SESSION['userId']));

?>

<a href="/logout.php" class="button">Log out</a>

<?php require_once('../includes/footer.php'); ?>
