<?php
	require_once('../../includes/functions.php');

	$pageTitle = "Webber's world Dashboard";
	show_header(array(
		"pageTitle" => $pageTitle,
		"pageDescription" => "Account settings for Dom Webber's work experience website."
	));

	require_once("../../includes/navbar.php");

	if(!empty($_SESSION['userId']) && !empty($_SESSION['userEmail'])):
		$logged = (new user())->get_account($_SESSION['userId'],$_SESSION['userEmail']);
	else:
		header("Location: /login.php");
	endif;

  if( isset($_POST['btn']) ){
    //Process and save preferences for settings
    user::setting($_POST);
  }
?>
<form action="" method="POST">
  Select your favorite color:
  <input type="color" id="color" name="color" value="#ff0000" onchange="document.getElementById('navbar-nav').style.backgroundColor = (this.value);">
  <input type="submit" name="btn">
</form>
